'''
COM3110         :  TEXT PROCESSING
Assignment      :  Document Retrieval 
Registration No.:  130197632
'''

from math import sqrt, log10


class Retrieve:
    def __init__(self, index, termWeighting):
        """Constructor for Retrieve Class.  
        Calculate the collection size and 
        select the function to measure distance
        according to termWeighting.
    
        Keyword arguments:
        index -- the index dictionary 
        termWeighting -- selected weighting scheme
        """       
        self.index = index       
        self.docsNum = self.collectionSize()    # num of docs in collection          
        if termWeighting == 'binary':           # binary 
            self.calculateDistance = self.distWithBinary
        elif termWeighting == 'tf':             # tf 
            self.calculateDistance = self.distWithTf
        else:                                   # tf-idf
            self.calculateDistance = self.distWithTfidf

    def forQuery(self, query):
        """Return a list of relevant documents in collection
        that match with query.
    
        Keyword arguments:
        query -- query being processed         
        """       
        scores = {}       
        candidates = self.candidatesForAQuery(query)   # set of document candidates
        distMeasure = self.calculateDistance           # avoid dots to speed-up code
        for docid in candidates:                      
            scores[docid] = distMeasure(query, docid)
        results = scores.keys()
        results.sort(reverse=True, key=lambda m: scores[m])
        return results     
  
    def distWithBinary(self, query, docid): 
        """Return a simplified cosin distance 
        between a document and a query using
        BINARY weighting scheme.
    
        Keyword arguments:
        query -- query being processed
        docid -- document being processed
        """       
        over = 0  # nominator
        for term in query:
            if term in self.index:
                if docid in self.index[term]:
                    over += 1
        squareSum = 0
        for term in self.index:
            if docid in self.index[term]:
                squareSum += 1
        under = sqrt(squareSum)  # denominator
        return over/under
   
    def distWithTf(self, query, docid): 
        """Return a simplified cosin distance 
        between a document and a query using
        TF weighting scheme.
    
        Keyword arguments:
        query -- query being processed
        docid -- document being processed
        """       
        over = 0  # nominator
        for term in query:
            if term in self.index:
                if docid in self.index[term]:
                    qi = query[term]              # count in query
                    di = self.index[term][docid]  # count in document
                    over += qi * di
        squareSum	= 0
        for term in self.index:
            if docid in self.index[term]:
                di = self.index[term][docid]
                squareSum += di**2
        under = sqrt(squareSum)  # denominator
        return over/under   
   
    def distWithTfidf(self, query, docid): 
        """Return a simplified cosin distance 
        between a document and a query using
        TF.IDF weighting scheme.
    
        Keyword arguments:
        query -- query being processed
        docid -- document being processed
        """       
        over = 0  # nominator
        squareSum = 0  
        for term in self.index:
            if docid in self.index[term]:
                df = len(self.index[term].keys())  # document frequency
                idf = log10(self.docsNum/df)             
                tf = self.index[term][docid]  # term frequency
                di = tf * idf
                squareSum += di**2
                if term in query:
                    qi = query[term] * idf
                    over += qi * di
        under = sqrt(squareSum)  # denominator  
        return over/under

    def collectionSize(self):
        """Iterate through index dictionary, find the 
        set of all docids in collection by taking union
		of all documents for a term and return the 
		length of resulting set. 
        """
        docs = set()
        for term in self.index:
            docids = self.index[term].keys()
            docs = docs | set(docids)     
        return len(docs)

    def candidatesForAQuery(self, query):
        """Return a set of documents that are 
		candidates (i.e. having at least one 
		common term with query) for a query. 
    
        Keyword arguments:
        query -- query being processed
        """
        candidates = set()
        for term in query:           
            if term in self.index:
                docs = set(self.index[term].keys())            
                candidates = candidates | docs
        return candidates       
