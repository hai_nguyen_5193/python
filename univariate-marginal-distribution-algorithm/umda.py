from __future__ import division  	# float division
from random import random
import numpy as np
import matplotlib.pyplot as plt
import operator, math

def umda(lmbda,mu,n,margin=0.5):
    p = np.zeros((1,n))[0]+0.5    # prob. model
    pop = np.zeros((lmbda,n))     # population
    iterations = 0
    while (True):
        iterations += 1
        for row in pop:           
            for col in xrange(n):
                row[col] = 1 if (random()<=p[col]) else 0
        sorted_pop = sort_pop(pop)
        for col in xrange(n):
            x_i = sum(int(b) for b in pop[:mu,col])
            if (x_i < margin):
                p[col] = round(margin/mu,2)
            elif (x_i > mu-margin):
                p[col] = round(1-margin/mu,2)
            else:
                p[col] = round(x_i/mu,2)
        best_f = sum(int(b) for b in sorted_pop[0,:])
        if (best_f == n): break
    return iterations
       
    
def sort_pop(pop):
    dic = {}
    for row in xrange(pop.shape[0]):
        dic[row] = sum(int(b) for b in pop[row,:])
    sorted_dic = sorted(dic.items(), key=operator.itemgetter(1),reverse=True)
    sorted_pop = np.empty_like(pop)
    for indx in xrange(len(sorted_dic)):
        sorted_pop[indx,:] = pop[sorted_dic[indx][0],:]
    return sorted_pop


def run(size):   
    aver_time = []
    for indx in range(size):
        time = 0
        n = indx+1
        mu = int(math.ceil(n/2))
        lmbda = 20  
        for i in range(20):                     
            time += umda(lmbda,mu,n)
        aver_time.append(round(time/20,2))
    return aver_time


res = run(16)
plt.plot([n+1 for n in xrange(16)], res)
plt.xlabel('length of bit-string')
plt.ylabel('#iterations')
plt.show()
