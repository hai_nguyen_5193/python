'''
We are considering the OneMax problem.
The algorithm uses the fitness function of OneMax.
The algorithm initially generates initial population of size POP_SIZE, each individual has size SOL_SIZE 
The mutator function mutates each bit independently with prob. p_m = 1/SOL_SIZE.
'''

from __future__ import division  	# float division
from random import random, randint
import sys
import matplotlib.pyplot as plt


FILE_NAME = 'population.txt'
POP_SIZE = 20  # size of the population
SOL_SIZE = 50  # length of bit-string


def fitness_evaluator(sol):
    """Calculate the fitness value for a given solution"""
    return sum(int(x) for x in sol if x.isdigit())


def tournament_selector(cand1,cand2):
    """Compare the fitness values of two candidates and then return the best"""
    return cand1 if (fitness_evaluator(cand1) >= fitness_evaluator(cand2)) else cand2


def bit_mutator(bit):
    """Mutate a bit with mutation prob. p_m"""
    p_m = 1/SOL_SIZE
    if (random() <= p_m): 
        return '0' if (bit=='1') else '1'
    else:
        return bit


def mutator(sol):
	"""Mutate each bit indepedently with prob p_m"""
	new_sol = [bit_mutator(x) for x in sol]
	return ''.join(new_sol[:SOL_SIZE])


def ini_pop():
	"""Read the input file to get initial population"""	
	with open(FILE_NAME, "r+") as file:
		ini_pop = []
		for line in file:
			ini_pop.append(line)
	file.close()
	return ini_pop


def best_fitness(pop):
    """Return the best fitness in the current population"""
    fitness_arr = map(lambda x : fitness_evaluator(x), pop)	
    return sorted(fitness_arr, reverse=True)[0]


def pop_generator(cur_pop):
    """Generate population for next iteration"""
    new_pop = []
    for i in xrange(POP_SIZE):
        indx1 = randint(0,POP_SIZE-1)  # index of candidate 1
        while True:
            indx2 = randint(0,POP_SIZE-1)  # index of candidate 2 
            if indx2 != indx1:   # two indices must be different
                break
        winner = tournament_selector(cur_pop[indx1],cur_pop[indx2])  # tournament
        mutated_winner = mutator(winner)  # mutation
        new_pop.append(mutated_winner)
    return new_pop


def initialize():
	"""Generate initial population, then stored in .txt file"""
	with open(FILE_NAME, "w") as file:
		for row in xrange(POP_SIZE):
			for indx in xrange(SOL_SIZE):
				file.write(str(randint(0,1)))
			file.write('\n')
	file.close()
	
	
def print_to_file(pop, iter, best_fitness):
	"""Print current value to the .txt file"""
	with open(FILE_NAME, "a") as file:
		file.write('*'*50 +'\n')
		file.write('Population at iteration '+ str(iter) +'\n') 
		file.write('Best fitness: '+ str(best_fitness)+ '\n')
		for indx in pop:
			file.write(indx+'\n')
	file.close()


def main():
    initialize()  # generate initial pop
    cur_pop = ini_pop()  # read initial pop into an array
    best_arr = []
    iter = 1
    while True:
        new_pop = pop_generator(cur_pop)
        best = best_fitness(new_pop)
        print_to_file(new_pop, iter, best)
        best_arr.append(best)
        if best==SOL_SIZE:  # found the optimum
            break
        iter += 1   # continue to next iteration
        cur_pop = new_pop    
    plt.plot(xrange(iter), best_arr, 'ro')
    plt.show()


main()